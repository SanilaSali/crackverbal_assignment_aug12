<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
   <head>
  	<%@ page isELIgnored="false" %>
      <meta charset="UTF-8">
      <title>Repositoty</title>
      
   </head>
   <body>
  <div>
  <h2><center>Repository Demo</center></h2>
  <div style="margin-left:160px; margin-bottom:30px;">
        <table width ="660" border="1" cellpadding="5">
            <caption><h3>Version History</h3></caption>
            <tr>
                <th width = "25%">Name</th>
                <th width = "10%">Version</th>
                <th width = "45%">Created Date</th>
                <th width = "15%">Created By</th>
            </tr>
            <c:if test="${not empty versionHistory}">
            <c:forEach var="file" items="${versionHistory}">
                <tr>
                    <td width = "25%"><c:out value="${file.document}" /></td>
                    <td width = "10%"><a href="DocumentServlet?param1=${file.version}&param2=${file.document}" >${file.version} </a> </td>
                    <td width = "45%"><c:out value="${file.createdDate}" /></td>
                    <td width = "15%"><c:out value="${file.createdBy}" /></td>
               </tr>
            </c:forEach>
            </c:if>
          </table>
    </div> 
  <div style="margin-left:160px">
     <form method="POST" action="DocumentServlet">
     	<Label> Enter Document name <i> (ending with .docx)</i> :</Label><input type ="text" name="docName" id="docId"> <button style="margin-top:10px;margin-left:40px" type="submit"> SAVE </button></br>
     	<c:if test="${not empty docContent}">
       <textarea style="margin-top:10px" name ="mydoc" rows="20" cols="80">${docContent}</textarea></br>
       </c:if>
       <c:if test="${empty docContent}">
       <textarea style="margin-top:10px" name ="mydoc" rows="20" cols="80"></textarea></br>
       </c:if>
       
      </form>
   </div>
   
  </div>
        </body>
</html>